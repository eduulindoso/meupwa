 [![pipeline status](https://gitlab.com/eduulindoso/meupwa/badges/master/pipeline.svg)](https://gitlab.com/eduulindoso/meupwa/-/commits/master)


# PWA de Hamburgueria

## O que é um PWA ???

Aplicativo Web que entrega a experiência de uso de app comum para os usuários usando as funcionalidades providas pelos recursos das páginas web e seus browsers. No fim verá que é apenas um website que é executado com alguns recursos. O PWA nos da opção de:

 - Instalar o sistema na tela do celuar
 - Acessar o mesmo offline
 - Acessar a câmera
 - Enviar notificações
 - Fazer atualização em background
