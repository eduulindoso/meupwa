const staticDevGumi = "dev-gumi-site-v1"
const assets = [
  "index.html",
  "js/app.js",
  "css/estilo.css",
  "images/hamb01.jpg",
  "images/hamb02.jpg",
  "images/hamb03.jpg",
  "images/promo.jpg",
]

self.addEventListener("install", installEvent => {
  installEvent.waitUntil(
    caches.open(staticDevGumi).then(cache => {
      //cache.addAll(assets)
      //cache.put(fetchEvent.request, response.clone());
    })
  )
}
)

self.addEventListener("fetch", fetchEvent => {
  fetchEvent.respondWith(
    caches.match(fetchEvent.request).then((resp) => {
      return resp || fetch(fetchEvent.request).then((response) => {
        // cache = caches.open(staticDevGumi);
        return caches.open(staticDevGumi).then((cache) => {
          cache.put(fetchEvent.request, response.clone());
          return response;
        });
      });
    })
  );
});
